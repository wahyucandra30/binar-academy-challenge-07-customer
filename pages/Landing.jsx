import Image from "next/image"
import LandingChart from "./LandingChart";

const Landing = () => {
    return (
        <div className="flex items-start justify-center w-full h-full py-24">
            <div className="flex flex-col md:flex-row items-start justify-between w-full max-w-[1920px] px-16">
                <div className="flex w-full md:w-fit">
                    <LandingChart width={500} height={500}/>
                </div>
                <div className="flex justify-end w-full md:w-1/2 h-full pr-20">
                    <div className="w-fit max-w-[468px] flex flex-col justify-center gap-6">
                        <h1 className="text-2xl font-bold">Best Car Rental for any kind of trip in (Lokasimu)!</h1>
                        <div className="flex flex-col gap-[18px]">
                            <p className="text-sm">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                                kondisi mobil baru,
                                serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                            </p>
                            <ul className="flex flex-col gap-5 text-sm">
                                <li className="flex items-center gap-4">
                                    <Image src={"/icon_checkmark.png"} width={"24px"} height={"24px"} />
                                    Sewa Mobil Dengan Supir di Bali 12 Jam
                                </li>
                                <li className="flex items-center gap-4">
                                    <Image src={"/icon_checkmark.png"} width={"24px"} height={"24px"} />
                                    Sewa Mobil Lepas Kunci di Bali 24 Jam
                                </li>
                                <li className="flex items-center gap-4">
                                    <Image src={"/icon_checkmark.png"} width={"24px"} height={"24px"} />
                                    Sewa Mobil Jangka Panjang Bulanan
                                </li>
                                <li className="flex items-center gap-4">
                                    <Image src={"/icon_checkmark.png"} width={"24px"} height={"24px"} />
                                    Gratis Antar - Jemput Mobil di Bandara
                                </li>
                                <li className="flex items-center gap-4">
                                    <Image src={"/icon_checkmark.png"} width={"24px"} height={"24px"} />
                                    Layanan Airport Transfer / Drop In Out
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Landing;