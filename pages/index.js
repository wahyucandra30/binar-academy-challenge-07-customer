import Head from 'next/head'
import Navbar from "./Navbar"
import Welcome from "./Welcome"
import Landing from "./Landing"
import Footer from './Footer'

export default function Home() {
  return (
    <>
      <Head>
        <title>Binar Car Rental</title>
        <meta name="description" content="Binar Car Rental Landing Page (NextJS)" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Welcome />
      <Landing />
      <Footer />
    </>
  )
}
